# SPASlides

A slides generator written in Smalltalk
## Installation
Simplest way to install is through `Iceberg`, or simply donwloading the repo and adding the `.st` files into your system browser
## Example

```smalltalk
"Initialize server"
server := SPAServer newAtPort: 1234.

"Add a presentation to it"
presentation := SPASPresentation new.
server presentation: presentation.

"Start server"
server start.

"Add slide to presentation"
presentation addSlide: (
		"Create slide"
		(SPASSlide new)
				"Add componets to head of slide"
				addToHead: (SPASTitle newWithText: 'SPASlides');
				addToHead: (SPASSubTitle newWithText: 'An SPA slides generator written in Pharo')
).

"Add another slide with a paragraph"
presentation addSlide: (
	(SPASSlide new)
		addToContent:(SPASParagraph newWithText: 'But why?')
).

"Add an onerdered list in a slide"
presentation addSlide: (
	(SPASSlide new)
		addToContent: (
			(SPASOrderedList new)
					addItem:(SPASListItem newWithText: 'Writing slides is an esential part of teaching');
					addItem:(SPASListItem newWithText: 'The dev team wanted to have a shot at developing in Pharo');
					addItem:(SPASListItem newWithText: '... beacause it was a fun thing to do :)')
				)
).
```
