Class {
	#name : #SPASComponentTests,
	#superclass : #TestCase,
	#category : #'SPASlides-Tests'
}

{ #category : #tests }
SPASComponentTests >> test01aTextComponentRendersItsText [

	|components|
	
	components := SPASTextComponent subclasses collect: [ :textComponent | textComponent newWithText: 'aText'].
	
	self assert: (components allSatisfy: [:component | component renderHTML matchesRegex: '.*aText.*'])
]

{ #category : #tests }
SPASComponentTests >> test02anImageComponentRendersItsSourceAndAlt [

	|component|
	
	component := SPASImage newWithSrc: 'aSrc' andAlt: 'anAlt'.
		
	self assert: (component renderHTML matchesRegex: '.*src="aSrc".*alt="anAlt".*')
]

{ #category : #tests }
SPASComponentTests >> test03aListItemsRendersItsItems [

	|components|
		
	components := SPASListComponent subclasses collect: [ :listComponent | (listComponent new) addItem: (SPASListItem newWithText: 'anItem')].
	
	self assert: (components allSatisfy: [:component | component renderHTML matchesRegex: '.*<li>anItem</li>.*'])
	
]
