Class {
	#name : #SPASlidesTest,
	#superclass : #TestCase,
	#category : #'SPASlides-Tests'
}

{ #category : #tests }
SPASlidesTest >> test01aNewPresentationIsEmpty [

	|presentation|
	
	presentation := SPASPresentation new.
	
	self assert: (presentation totalSlides) equals: 0
	
	
]

{ #category : #tests }
SPASlidesTest >> test02aPresentationAcceptsSlides [

	|presentation|
	
	presentation := SPASPresentation new.
	
	presentation addSlide: (SPASSlide new).
	
	self assert: (presentation slides size) equals: 1
]

{ #category : #tests }
SPASlidesTest >> test03aNewSlideHasNoHeadComponents [

	|slide|
	
	slide := SPASSlide new.
	
	self assert: (slide totalHeadComponents) equals: 0
]

{ #category : #tests }
SPASlidesTest >> test04aNewSlideHasNoContentComponents [

	|slide|
	
	slide := SPASSlide new.
	
	self assert: (slide totalContentComponents) equals:0
]

{ #category : #tests }
SPASlidesTest >> test05aSlideAcceptsComponentsOnHead [

	|slide|
	
	slide := SPASSlide new.
	
	slide addToHead: (SPASComponent new).
	
	self assert: (slide head totalComponents) equals:1
]

{ #category : #tests }
SPASlidesTest >> test06aSlideAcceptsComponentsOnContent [

	|slide|
	
	slide := SPASSlide new.
	
	slide addToContent: (SPASComponent new).
	
	self assert: (slide content totalComponents) equals:1
]

{ #category : #tests }
SPASlidesTest >> test07anEmptySlideRendersAnEmptyDiv [

	|slide|
	
	slide := SPASSlide new.
	
	self assert: (slide renderHTML) equals: '<div class="slide"></div>'
]

{ #category : #tests }
SPASlidesTest >> test08addingComponentToHeadRendersAHeadSection [

	|slide|
	
	slide := SPASSlide new.
	
	slide addToHead: (SPASTitle newWithText: 'SPASlides').
	
	self assert: (slide renderHTML matchesRegex: '<div class="slide"><div class="slide-head">.*</div></div>')
]

{ #category : #tests }
SPASlidesTest >> test09addingComponentToContentRendersAComponentSection [

	|slide|
	
	slide := SPASSlide new.
	
	slide addToContent: (SPASTitle newWithText: 'SPASlides').
	
	self assert: (slide renderHTML matchesRegex: '<div class="slide"><div class="slide-content">.*</div></div>')
]

{ #category : #tests }
SPASlidesTest >> test10addingATitleToHeadSectionRendersAnH1 [

	|slide|
	
	slide := SPASSlide new.
	
	slide addToHead: (SPASTitle newWithText: 'SPASlides').
	
	self assert: (slide renderHTML matchesRegex: '.*<h1>.*</h1>.*') 
]

{ #category : #tests }
SPASlidesTest >> test11addingASubTitleToHeadSectionRendersAnH2 [

	|slide|
	
	slide := SPASSlide new.
	
	slide addToHead: (SPASSubTitle newWithText: 'SPsASlides').
	
	self assert: (slide renderHTML matchesRegex: '.*<h2>.*</h2>.*') 
]

{ #category : #tests }
SPASlidesTest >> test12addingAParagraphToContentRendersAP [

	|slide|
	
	slide := SPASSlide new.
	
	slide addToContent: (SPASParagraph newWithText: 'SPsASlides').
	
	self assert: (slide renderHTML matchesRegex: '.*<p>.*</p>.*') 
]

{ #category : #tests }
SPASlidesTest >> test13addingAnImageRendersAnImg [

	|slide aSrc aRegexString|
	
	aSrc := 'http://lorempixel.com/output/cats-q-c-640-480-3.jpg'.

	slide := SPASSlide new.
	
	slide addToContent: (SPASImage newWithSrc: aSrc andAlt: 'anImage').
	
	aRegexString :=  '.*<img.*/>.*'.
	
	self assert: (slide renderHTML matchesRegex: aRegexString)
]

{ #category : #tests }
SPASlidesTest >> test14addingAnUnorderedListRendersAUl [

	|slide|

	slide := SPASSlide new.
	
	slide addToContent: (SPASUnorderedList new).
	
	self assert: (slide renderHTML matchesRegex: '.*<ul>.*</ul>.*' )
]

{ #category : #tests }
SPASlidesTest >> test15addingAnOrderedListRendersAnOl [

	|slide|

	slide := SPASSlide new.
	
	slide addToContent: (SPASOrderedList new).
	
	self assert: (slide renderHTML matchesRegex: '.*<ol>.*</ol>.*' )
]

{ #category : #tests }
SPASlidesTest >> test16addingACodeSectionRendersAPreCode [

	|slide|

	slide := SPASSlide new.
	
	slide addToContent: (SPASCode newWithText:'char *a = "Hello world!"').
	
	self assert: (slide renderHTML matchesRegex: '.*<pre><code>.*</code></pre>.*' )
]

{ #category : #tests }
SPASlidesTest >> test17addingComponentsCanBeChained [

	|slide|

	slide := SPASSlide new.
	
	slide addToHead: (SPASTitle newWithText: 'A title');
			addToHead: (SPASSubTitle newWithText: 'A subtitle');
			addToContent: (SPASCode newWithText:'char *a = "Hello world!"').
	
	self assert: slide totalHeadComponents equals: 2.
	self assert: slide totalContentComponents equals:1.
]

{ #category : #tests }
SPASlidesTest >> test18aSlideCanBeEmptiedOut [

	|slide|

	slide := SPASSlide new.
	
	slide addToHead: (SPASTitle newWithText: 'A title');
			addToHead: (SPASSubTitle newWithText: 'A subtitle');
			addToContent: (SPASCode newWithText:'char *a = "Hello world!"').
	
	self assert: slide totalHeadComponents equals: 2.
	self assert: slide totalContentComponents equals:1.
	
	slide emptyHead.
	slide emptyContent.
	
	self assert: slide totalHeadComponents equals: 0.
	self assert: slide totalContentComponents equals:0.
]
