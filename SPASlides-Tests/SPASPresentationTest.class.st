Class {
	#name : #SPASPresentationTest,
	#superclass : #TestCase,
	#category : #'SPASlides-Tests'
}

{ #category : #tests }
SPASPresentationTest >> test01presentationRendersItsSlides [

	|presentation|
	
	presentation := SPASPresentation new.
		
	presentation addSlide: (SPASSlide new).
	presentation addSlide: (SPASSlide new).
	presentation addSlide: (SPASSlide new).
	
	self assert: ((presentation renderHTML regex: 'class="slide( current)?"' matchesCollect: [:slide | slide]) size) equals: 3
]

{ #category : #tests }
SPASPresentationTest >> test02firstSlideIsCurrent [

	|presentation|
	
	presentation := SPASPresentation new.
		
	presentation addSlide: (SPASSlide new).
	presentation addSlide: (SPASSlide new).
	presentation addSlide: (SPASSlide new).
	
	self assert: ((presentation renderHTML regex: 'class="slide current"' matchesCollect: [:slide | slide]) size) equals: 1
]

{ #category : #tests }
SPASPresentationTest >> test03addingSlidesCanBeChained [

	|presentation|
	
	presentation := SPASPresentation new.
		
	presentation addSlide: (SPASSlide new); 
						addSlide: (SPASSlide new);
						addSlide: (SPASSlide new).
	
	self assert: presentation totalSlides equals: 3
]

{ #category : #tests }
SPASPresentationTest >> test04aPresentationCanBeEmptiedOut [

	|presentation|
	
	presentation := SPASPresentation new.
		
	presentation addSlide: (SPASSlide new); 
						addSlide: (SPASSlide new);
						addSlide: (SPASSlide new).
	
	self assert: presentation totalSlides equals: 3.
	
	presentation empty.
	
	self assert: presentation totalSlides equals: 0
	
	
]
