"
Base class to represent components
"
Class {
	#name : #SPASComponent,
	#superclass : #SPASElement,
	#category : #SPASlides
}

{ #category : #rendering }
SPASComponent >> renderHTML [
	self subclassResponsibility
]
