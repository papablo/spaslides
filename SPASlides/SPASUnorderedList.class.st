Class {
	#name : #SPASUnorderedList,
	#superclass : #SPASListComponent,
	#category : #SPASlides
}

{ #category : #rendering }
SPASUnorderedList >> renderHTML [
	^ '<ul>',('' join: (items collect:[:item | item renderHTML])),'</ul>'
]
