Class {
	#name : #SPASPresentation,
	#superclass : #SPASElement,
	#instVars : [
		'slides'
	],
	#category : #SPASlides
}

{ #category : #'slide-manipulation' }
SPASPresentation >> addSlide: aSPASSlide [ 
	slides add: aSPASSlide 
]

{ #category : #'slides-manipulation' }
SPASPresentation >> empty [
	slides := OrderedCollection new.
]

{ #category : #rendering }
SPASPresentation >> headHTML [
	^ '<!DOCTYPE html> <html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="ie=edge" />
        <!--Styles-->

        <style>',
(self styles),
		'</style>

        <title>Document</title>
    </head>
    <body>
'
]

{ #category : #initialization }
SPASPresentation >> initialize [ 
	slides := OrderedCollection new.
]

{ #category : #scripting }
SPASPresentation >> jsScript [
	^ 'const slides = document.querySelectorAll(".slide");

const numerateSlides = slides => {

  slides.forEach((slide, index) => {
    slide.setAttribute("id", index)
  })

}

numerateSlides(slides)

const currentSlide = slides => {
  let current;
  slides.forEach((slide, index) => {
    if (slide.classList.contains("current")) {
      current = index;
    }
  });
  return current;
};

const nextSlide = slides => {
  // Go to the next slide from `slides`
  // whichever it is

  const current = currentSlide(slides);
  const last = slides.length;

  if (current != last - 1) {
    slides[current].classList.toggle("current");
    slides[current + 1].classList.toggle("current");
  }
};

const previousSlide = slides => {
  const current = currentSlide(slides);

  if (current != 0) {
    slides[current].classList.toggle("current");
    slides[current - 1].classList.toggle("current");
  }
};

document.onkeydown = function(event) {
  switch (event.keyCode) {
    case 37:
      previousSlide(slides);
      break;
    case 39:
      nextSlide(slides);
      break;
  }
};
'
]

{ #category : #rendering }
SPASPresentation >> renderHTML [
	|aRenderedSlides|
	
	aRenderedSlides := '' join: (slides collectWithIndex: [:slide :i | slide renderHTMLatSlide: i]).
	
	^ (self headHTML),aRenderedSlides,(self tailHTML)
]

{ #category : #exporting }
SPASPresentation >> saveHTMLWithName:aFilename in: aPath [

	|aRenderedPresentation stream|
	
	aRenderedPresentation := self renderHTML.


	stream := ((aPath asFileReference) / aFilename) writeStream.
	
	stream nextPutAll: aRenderedPresentation.
	
	stream close.
]

{ #category : #rendering }
SPASPresentation >> scriptTag [
	^ '<script>',(self jsScript),'
        </script>'
]

{ #category : #accesing }
SPASPresentation >> slides [
	^ slides
]

{ #category : #styling }
SPASPresentation >> styles [
	^ '* {
  margin: 0;
  padding: 0;
  box-sizing: border-box;
  font-family: "Courier New", Courier, monospace;
}

.slide {
  display: none;
  height: 100vh;
}

.current {
  display: flex;
  flex-direction: column;
  justify-content: center;
  padding: 1em;
  animation: drop 1s ease;
}

.slide-head {
  display: flex;
  align-items: center;
  flex-direction: column;
  margin-bottom: 1em;
}

.slide-content {
  padding-left: 1.5em;
  margin-left: 1em;
  margin-top: 1em;
  margin-right: 1em;
  font-size: 2em;
  height:40%

}

h1 {
  font-size: 7em;
}

h2 {
  font-size: 3em;
}

a {
  text-decoration: none;
}

.slide-content img {
    display:block;
    margin:auto;
    height:100%
}


pre code {
    background-color: whitesmoke;
  border: 1px solid #999;
  display: block;
  padding: 0;
}

@keyframes drop {
	0% {
		opacity: 0;
		transform: translateX(-80px);
	}
	100% {
		opacity: 1;
		transform: translateX(0px);
	}
}
'
]

{ #category : #rendering }
SPASPresentation >> tailHTML [
	^ (self scriptTag),'
    </body>
</html>
'
]

{ #category : #'as yet unclassified' }
SPASPresentation >> totalSlides [
	^slides size
]
