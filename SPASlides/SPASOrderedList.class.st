Class {
	#name : #SPASOrderedList,
	#superclass : #SPASListComponent,
	#category : #SPASlides
}

{ #category : #rendering }
SPASOrderedList >> renderHTML [
	^'<ol>',('' join: (items collect:[:item | item renderHTML])),	'</ol>' 
]
