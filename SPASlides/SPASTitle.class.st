Class {
	#name : #SPASTitle,
	#superclass : #SPASTextComponent,
	#category : #SPASlides
}

{ #category : #rendering }
SPASTitle >> renderHTML [
	^'<h1>',text,'</h1>'
]
