Class {
	#name : #SPASSubTitle,
	#superclass : #SPASTextComponent,
	#category : #SPASlides
}

{ #category : #rendering }
SPASSubTitle >> renderHTML [
	^ '<h2>',text,'</h2>'
]
