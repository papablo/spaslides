Class {
	#name : #SPASImage,
	#superclass : #SPASComponent,
	#instVars : [
		'src',
		'alt'
	],
	#classInstVars : [
		'src'
	],
	#category : #SPASlides
}

{ #category : #initialization }
SPASImage class >> newWithSrc: aSrc andAlt: anAlt [

	|anImageComponent|
	
	anImageComponent := self new.
	
	anImageComponent src: aSrc.
	anImageComponent alt: anAlt.
	
	^ anImageComponent
]

{ #category : #accessing }
SPASImage >> alt: anObject [
	alt := anObject
]

{ #category : #rendering }
SPASImage >> renderHTML [
	^ '<img src="',src,'" alt="',alt,'"/>'
]

{ #category : #accessing }
SPASImage >> src: anObject [
	src := anObject
]
