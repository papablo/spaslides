Class {
	#name : #SPASSlide,
	#superclass : #SPASElement,
	#instVars : [
		'head',
		'content'
	],
	#category : #SPASlides
}

{ #category : #'component-manipulation' }
SPASSlide >> addToContent: aSPASComponent [ 
	content addComponent: aSPASComponent
]

{ #category : #'component-manipulation' }
SPASSlide >> addToHead: aSPASComponent [ 
	head addComponent: aSPASComponent.
]

{ #category : #accessing }
SPASSlide >> content [
	^ content
]

{ #category : #'as yet unclassified' }
SPASSlide >> emptyContent [
	content empty
]

{ #category : #'component-manipulation' }
SPASSlide >> emptyHead [
	head empty
]

{ #category : #accessing }
SPASSlide >> head [
	^ head
]

{ #category : #initialization }
SPASSlide >> initialize [ 
	head := SPASHead new.
	content := SPASContent new.
]

{ #category : #rendering }
SPASSlide >> renderHTML [
	|headSection contentSection|
	
	headSection := head renderHTML.
	contentSection := content renderHTML.
	
	^'<div class="slide">',headSection,contentSection,'</div>'
]

{ #category : #rendering }
SPASSlide >> renderHTMLatSlide: aSlideNumber [

	|headSection contentSection slideCssClasses |
	
	slideCssClasses:='slide'.
	
	headSection := head renderHTML.
	contentSection := content renderHTML.
	
	(aSlideNumber = 1) ifTrue:[
		slideCssClasses := slideCssClasses, ' current'
	].

	^'<div id="',(aSlideNumber asString),'" class="',slideCssClasses,'">',headSection,contentSection,'</div>'
]

{ #category : #sections }
SPASSlide >> totalContentComponents [
	^ content totalComponents 
]

{ #category : #'component-totalization' }
SPASSlide >> totalHeadComponents [
	^ head totalComponents
]
