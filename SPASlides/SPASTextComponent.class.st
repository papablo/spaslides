Class {
	#name : #SPASTextComponent,
	#superclass : #SPASComponent,
	#instVars : [
		'text'
	],
	#category : #SPASlides
}

{ #category : #'instance creation' }
SPASTextComponent class >> newWithText: aString [ 
	
	|component|
	
	component := (self new).
	component text: aString.
	
	^ component 
]

{ #category : #rendering }
SPASTextComponent >> renderHTML [
	self subclassResponsibility 
]

{ #category : #accessing }
SPASTextComponent >> text [
	^ text
]

{ #category : #accessing }
SPASTextComponent >> text: anObject [
	text := anObject
]
