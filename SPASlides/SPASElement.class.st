"
Base Element for all elements in  a presentation 

Subclasse of this should implement the renderHTML message
"
Class {
	#name : #SPASElement,
	#superclass : #Object,
	#category : #SPASlides
}

{ #category : #rendering }
SPASElement >> renderHTML [
	self subclassResponsibility 
]
