Class {
	#name : #SPASListComponent,
	#superclass : #SPASComponent,
	#instVars : [
		'items'
	],
	#classInstVars : [
		'items'
	],
	#category : #SPASlides
}

{ #category : #'item manipulation' }
SPASListComponent class >> addItem: aSPASListItem [ 
	items add: aSPASListItem
]

{ #category : #'item manipulation' }
SPASListComponent >> addItem: aSPASListItem [ 
	items add: aSPASListItem 
]

{ #category : #initialization }
SPASListComponent >> initialize [
	items := OrderedCollection new.
]

{ #category : #rendering }
SPASListComponent >> renderHTML [
	self subclassResponsibility
]
