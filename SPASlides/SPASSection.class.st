Class {
	#name : #SPASSection,
	#superclass : #SPASElement,
	#instVars : [
		'components',
		'cssClasses'
	],
	#category : #SPASlides
}

{ #category : #'component-manipulation' }
SPASSection >> addComponent: aSPASTitle [ 
	components add: aSPASTitle 
]

{ #category : #requirements }
SPASSection >> empty [
	components := OrderedCollection new.
]

{ #category : #initialization }
SPASSection >> initialize [
	components := OrderedCollection new.
	cssClasses := ''
]

{ #category : #rendering }
SPASSection >> renderHTML [
	components isEmpty ifFalse:[
		|renderedComponents|
		renderedComponents:= '' join: (components collect:[:component | component renderHTML]).
		 
		^ '<div class="',('' join:cssClasses),'">',renderedComponents,'</div>'
	] ifTrue: [ 
		^ ''
	]
	
]

{ #category : #accessing }
SPASSection >> totalComponents [
	^components size
]
