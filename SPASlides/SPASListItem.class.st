Class {
	#name : #SPASListItem,
	#superclass : #SPASTextComponent,
	#category : #SPASlides
}

{ #category : #rendering }
SPASListItem >> renderHTML [
	^'<li>',text,'</li>'
]
