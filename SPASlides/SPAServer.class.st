Class {
	#name : #SPAServer,
	#superclass : #Object,
	#instVars : [
		'port',
		'presentation',
		'server'
	],
	#category : #SPASlides
}

{ #category : #'instance creation' }
SPAServer class >> newAtPort: aPort [

	|aServer|
	
	aServer := self new.
	
	aServer port: aPort.	
	
	^ aServer
]

{ #category : #accesing }
SPAServer >> port: aPortNumber [ 
	port := aPortNumber
]

{ #category : #accesing }
SPAServer >> presentation [
	^ presentation
]

{ #category : #accessing }
SPAServer >> presentation: aPresentation [

	presentation := aPresentation 
]

{ #category : #accessing }
SPAServer >> start [
	server := (ZnServer startDefaultOn: port) 
    onRequestRespond: [ :request | 
        ZnResponse ok: (ZnEntity html: (presentation renderHTML)) ].

	^server
]

{ #category : #accessing }
SPAServer >> stop [
	"stop server"
	server isRunning ifTrue:[
		server stop.
	]

]
