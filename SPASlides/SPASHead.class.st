Class {
	#name : #SPASHead,
	#superclass : #SPASSection,
	#category : #SPASlides
}

{ #category : #initialization }
SPASHead >> initialize [ 
	super initialize.
	
	cssClasses := 'slide-head'
]
