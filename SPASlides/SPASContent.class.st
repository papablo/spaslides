Class {
	#name : #SPASContent,
	#superclass : #SPASSection,
	#category : #SPASlides
}

{ #category : #initialization }
SPASContent >> initialize [ 
	super initialize.
	
	cssClasses := 'slide-content'
]
